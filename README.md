# hajnyon.cz

Repository containing personal resume following [JSON resume](https://jsonresume.org/) initiative. Features generating resume in `html`, `pdf` and `json` formats.

## Usage

Edit resume data in `./data/resume.json` and then generate resume in JSON, HTML and PDF formats to `./public` folder.

```bash
npm ci
npm run build # builds all
npm run build:json # builds only JSON
npm run build:html # builds only JSON & HTML
npm run build:pdf # builds only JSON & PDF
```

### Deployment

Deploys `./public` folder to Cloudflare Pages using [Wrangler](https://github.com/cloudflare/wrangler). Needs API token and user ID or CLI login. See `./wrangler.json` for configuration.

```bash
npx wrangler pages deploy
```

## Credits

- JSON
    - https://jsonresume.org - uses JSON schema for input data
- HTML
    - https://github.com/owickstrom/the-monospace-web - used as a CSS theme
    - https://mustache.github.io - used for templating
    - https://stephango.com/flexoki - used for color palette
    - https://www.npmjs.com/package/html-minifier - used for HTML minification
    - https://lightningcss.dev - used for CSS minification
- PDF
    - https://pdfkit.org - used for PDF generation in node
