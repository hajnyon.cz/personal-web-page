{
	"basics": {
		"name": "Ondřej Hajný",
		"label": "Software developer",
		"picture": "https://hajnyon.cz/assets/images/me.webp",
		"email": "hajnyon@gmail.com",
		"website": "https://hajnyon.cz",
		"summary": "I'm a software developer who likes to build stuff. Currently interested mainly in frontend development using modern technologies.",
		"location": {
			"city": "Brno",
			"countryCode": "cz",
			"region": "South Moravia"
		},
		"profiles": [
			{
				"network": "GitLab",
				"username": "hajnyon",
				"url": "https://gitlab.com/hajnyon/"
			},
			{
				"network": "GitHub",
				"username": "hajnyon",
				"url": "https://github.com/hajnyon/"
			},
			{
				"network": "Twitter",
				"username": "@hajnyon",
				"url": "https://twitter.com/hajnyon/"
			},
			{
				"network": "Linkedin",
				"username": "hajnyon",
				"url": "https://linkedin.com/in/hajnyon/"
			},
			{
				"network": "Blog",
				"username": "hajnyon.log",
				"url": "https://blog.hajnyon.cz/"
			}
		]
	},
	"work": [
		{
			"company": "Cookielab",
			"position": "Fullstack developer",
			"website": "https://cookielab.io",
			"startDate": "2022-09-01",
			"endDate": "",
			"summary": "My journey in Cookielab started with working in a small team for Kiwi.com delivering frontend parts of the product. Then I contributed for various smaller projects. For a few months I helped bootstrap a \"Influencers\" product for a media research tool Ravineo. My last project here is Carvago, a platform for aggregating used cars to sell online.",
			"highlights": [
				"[Carvago] Introducing a new product through MVP to production ready state (Next.js, ChakraUI, jotai, jest)",
				"[Ravineo] Server part for collecting and processing data (GCP, Node.js, Fastify)",
				"[Ravineo] Web application with backend-for-frontend and user facing interface (Next.js, shadcn/ui, trpc, kysely)",
				"[Other] product listing web application (Next.js, shadcn/ui, Docker, Postgres, kysely)",
				"[Other] GUI for a small utility application gathering info from RFID chips (Solid.js, TailwindCSS, MSW)",
				"[Perfect Canteen] UI for self-serving meals machine (React.js, AWS AppSync, MSW, Playwright, vitest)",
				"[Kiwi.com] Iterating over a feature in MVP phase moving it to a standalone working product (React.js, Next.js, Orbit, GraphQL, jest, Cypress)"
			]
		},
		{
			"company": "FIEDLER AMS",
			"position": "Fullstack developer",
			"website": "http://fiedler.company/",
			"startDate": "2013-02-01",
			"endDate": "2022-08-31",
			"summary": "FIEDLER is a small company developing IoT devices used in meteorology. Focusing mainly on frontend I was part of team developing newly designed cloud platform for data access and device configuration.",
			"highlights": [
				"frontend for Electron application (Aurelia.js, Electron)",
				"frontend SPA (Aurelia.js, REST API, WS, jest, Cypress)",
				"built custom reusable Drupal modules"
			]
		}
	],
	"education": [
		{
			"institution": "University of South Bohemia",
			"area": "Faculty of Science (informatics)",
			"url": "https://prf.jcu.cz/",
			"studyType": "Bachelor",
			"startDate": "2019-10-01",
			"endDate": "2024-06-27"
		},
		{
			"institution": "Czech Technical University",
			"area": "Faculty of Informatics (unfinished)",
			"url": "https://fit.cvut.cz/",
			"studyType": "Bachelor",
			"startDate": "2014-10-01",
			"endDate": "2018-12-01"
		}
	],
	"skills": [
		{
			"name": "Frontend",
			"keywords": [
				"HTML",
				"CSS",
				"SCSS",
				"JavaScript",
				"TypeScript",
				"Figma",
				"React.js",
				"Next.js",
				"Solid.js",
				"Cypress",
				"Playwright",
				"vitest",
				"jest"
			]
		},
		{
			"name": "Backend",
			"keywords": ["Node", "Next.js", "PHP", "Postgres", "MySQL"]
		},
		{
			"name": "Devops",
			"keywords": ["Docker", "GitLab CI/CD"]
		},
		{
			"name": "Utilities",
			"keywords": ["git", "npm", "linux", "bash"]
		}
	],
	"languages": [
		{
			"language": "Czech",
			"fluency": "native"
		},
		{
			"language": "English",
			"fluency": "active"
		},
		{
			"language": "German",
			"fluency": "passive"
		}
	],
	"interests": [
		{
			"name": "Sports",
			"keywords": ["football", "running", "hiking"]
		},
		{
			"name": "3D printing",
			"keywords": [
				"modeling in OpenSCAD and Fusion 360",
				"printing on Prusa Mini+ or Ender 3 Pro"
			]
		}
	],
	"projects": [
		{
			"name": "SCADA screen for water industry usage",
			"description": "Bachelor thesis project, graded excellent. HMI part of SCADA system written in Aurelia.js",
			"roles": ["developer"],
			"url": "http://wstag.jcu.cz/StagPortletsJSR168/CleanUrl?urlid=prohlizeni-prace-detail&praceIdno=66518",
			"type": "SPA",
			"entity": "closed source",
			"highlights": [
				"WS communication",
				"dynamic SVG rendering",
				"software design",
				"user testing"
			]
		},
		{
			"name": "SK Rudolfov - soccer club's website",
			"description": "Presentation website for local sports club. Written in Next.js using Wordpress as a headless CMS.",
			"roles": ["fullstack"],
			"url": "https://skrudolfov.cz/",
			"type": "website",
			"entity": "closed source",
			"highlights": ["GraphQL API communication", "TailwindCSS styling"]
		},
		{
			"name": "Šlichta - restaurant's menus",
			"description": "Website aggregating favorite restaurant's menus at one place written in Next.js.",
			"roles": ["frontend"],
			"url": "https://slichta.vercel.app/",
			"type": "website",
			"entity": "open source",
			"highlights": [
				"scraper for various websites",
				"API setup between Apify scraper, GitLab CI/CD and Next.js application"
			]
		},
		{
			"name": "Middleman UIkit documentation template",
			"description": "Theme template for static site generator Middleman. Mainly for documentation purposes, supports few layouts and some useful tools. Ready for GitLab static site editor.",
			"roles": ["frontend", "designer"],
			"url": "https://github.com/hajnyon/middleman-uikit-documentation-template",
			"type": "template",
			"entity": "open source",
			"highlights": [
				"creating template using ruby and Middleman tools",
				"designing theme similar to other "
			]
		},
		{
			"name": "UIkit icons extended",
			"description": "Extended icons pack for UIkit.",
			"roles": ["designer"],
			"url": "https://hajnyon.gitlab.io/uikit-icons-extended/",
			"type": "library",
			"entity": "open source",
			"highlights": [
				"icons design in Figma",
				"continuous deployment using Figma API and GitLab CI/CD"
			]
		}
	],
	"publications": [
		{
			"name": "Mock Service Worker - usage for development and testing",
			"publisher": "Cookielab.io",
			"releaseDate": "2024-10-07",
			"url": "https://www.cookielab.io/blog/mock-service-worker-vyuziti-pro-testovani-i-vyvoj",
			"summary": "Presentation of MSW, a tool for mocking requests in development and testing of FE applications. Presented at Frontkon 2024 conference."
		}
	],
	"meta": {
		"canonical": "https://hajnyon.cz/v1/resume.json",
		"lastModified": "2025-01-19T20:09:21.944Z",
		"version": "v1.4.0"
	}
}
