# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.5.0] - 2025-01-21

### Added

- publications section
- biome for linting and formatting

### Changed

- CV work section summarized by job
- build script
- mustache templates converted to a single file
- added minification (HTML, CSS, images)
- fonts added locally
- changed deployment to Cloudflare Pages
- PDF generation using node

## [1.4.0] - 2024-09-07

## Added

- current work position

### Changed

- web theme - monospace
- web profile picture

## [1.3.0] - 2024-06-16

### Added

- current project
- finished education

### Changed

- jobs descriptions
- tools used

## [1.2.3] - 2023-09-21

### Changed

- Cookielab job description

## [1.2.2] - 2022-07-26

## Added

- skrudolfov.cz and slichta projects
- Cookielab job

## Changed

- migrated to node@16
- upgraded dependencies

## Fixed

- npm registry

## [1.1.1] - 2020-11-01

### Added

- GitHub link
- Middleman UIkit documentation template

## [1.0.1] - 2020-04-23

### Fixed

- typo

## [1.0.0] - 2020-03-08

### Added

- initial release
