import { readFile, writeFile } from "node:fs/promises";
import { OUTPUT_VERSION, getPathFromRoot } from "../utils.js";

const VERSION = process.env.npm_package_version;

const getResumeData = async () => {
	const resume = await readFile(getPathFromRoot("data/resume.json"), "utf8");
	return {
		resume: JSON.parse(resume),
		urls: {
			pdf: "/v1/resume.pdf",
			json: "/v1/resume.json",
			blog: "https://blog.hajnyon.cz",
			source: "https://gitlab.com/hajnyon.cz/personal-web-page",
		},
	};
};

export async function buildJson() {
	const data = await getResumeData();

	data.resume.meta.version = `v${VERSION}`;
	data.resume.meta.lastModified = new Date().toISOString();

	await writeFile(
		getPathFromRoot(`${OUTPUT_VERSION}/resume.json`),
		JSON.stringify(data.resume),
	);

	return data;
}
