import { dirname, join } from "node:path";
import { fileURLToPath } from "node:url";

export const getPathFromRoot = (path) => {
	const __dirname = dirname(fileURLToPath(import.meta.url));
	return join(__dirname, "..", path);
};

export const OUTPUT = "./public";
export const OUTPUT_VERSION = "./public/v1";
export const OUTPUT_WELL_KNOWN = "./public/.well-known";
