import { createWriteStream } from "node:fs";
import PDFDocument from "pdfkit";
import { OUTPUT_VERSION, getPathFromRoot } from "../utils.js";

const stripLink = (text) => text.replace("https://", "");

const fontSizes = {
	title: 20,
	subtitle: 16,
	text: 12,
	small: 10,
};

const fontColors = {
	primary: "#100F0F",
	secondary: "#656565",
};

const createLeftColumn = (doc, data) => {
	doc.y += 10;
	const contentYStart = doc.y;
	doc
		.fontSize(fontSizes.subtitle)
		.fillColor(fontColors.primary)
		.text("Reach me at: ");
	doc
		.fontSize(fontSizes.text)
		.fillColor(fontColors.secondary)
		.text("E-mail: ", { continued: true })
		.fillColor(fontColors.primary)
		.text(data.resume.basics.email, {
			link: `mailto:${data.resume.basics.email}`,
			underline: true,
		});
	doc
		.fontSize(fontSizes.text)
		.fillColor(fontColors.secondary)
		.text("Website: ", { continued: true })
		.fillColor(fontColors.primary)
		.text(stripLink(data.resume.basics.website), {
			link: data.resume.basics.website,
			underline: true,
		});
	const linkedin = data.resume.basics.profiles.find(
		(profile) => profile.network === "Linkedin",
	);
	doc
		.fontSize(fontSizes.text)
		.fillColor(fontColors.secondary)
		.text("Linkedin: ", { continued: true })
		.fillColor(fontColors.primary)
		.text(`in/${linkedin.username}`, {
			link: linkedin.url,
			underline: true,
		});

	doc.y += 10;
	doc.fontSize(fontSizes.subtitle).text("Education");
	for (const ed of data.resume.education) {
		doc
			.fillColor(fontColors.secondary)
			.fontSize(fontSizes.text)
			.text(ed.institution, {
				columns: 1,
				width: 230,
				underline: true,
				link: ed.url,
			});
		doc.fontSize(fontSizes.small).fillColor(fontColors.primary).text(ed.area);
		doc
			.fontSize(fontSizes.small)
			.fillColor(fontColors.primary)
			.text(`${ed.studyType} `, { continued: true, columns: 1, width: 230 })
			.fillColor(fontColors.secondary)
			.text(`  (${ed.startDate} — ${ed.endDate})`);
		doc.y += 5;
	}

	doc.y += 10;
	doc.fontSize(fontSizes.subtitle).fillColor(fontColors.primary).text("Skills");
	for (const skill of data.resume.skills) {
		doc
			.fillColor(fontColors.secondary)
			.fontSize(fontSizes.text)
			.text(`${skill.name}:`, { columns: 1, width: 230 });
		doc
			.fillColor(fontColors.primary)
			.fontSize(fontSizes.small)
			.text(skill.keywords.join(", "), { columns: 1, width: 230 });
		doc.y += 5;
	}

	doc.y += 10;
	doc.fontSize(fontSizes.subtitle).text("Languages");
	for (const lang of data.resume.languages) {
		doc
			.fillColor(fontColors.primary)
			.fontSize(fontSizes.text)
			.text(`${lang.language}: `, {
				continued: true,
				columns: 1,
				width: 230,
				align: "left",
			})
			.fillColor(fontColors.secondary)
			.text(lang.fluency);
		doc.y += 5;
	}

	doc.y += 10;
	doc
		.fontSize(fontSizes.subtitle)
		.fillColor(fontColors.primary)
		.text("Interests");
	doc
		.fillColor(fontColors.secondary)
		.fontSize(fontSizes.text)
		.text(data.resume.interests.map((interest) => interest.name).join(", "), {
			columns: 1,
			width: 230,
		});

	return contentYStart;
};

/**
 *
 * @param {*} doc
 * @param {*} data
 * @param {number} contentYStart Y position of the left column, where the right column starts
 */
const createRightColumn = (doc, data, contentYStart) => {
	doc
		.fontSize(fontSizes.subtitle)
		.fillColor(fontColors.primary)
		.text("Work", 250, contentYStart);

	for (const job of data.resume.work) {
		const titleY = doc.y;
		const tmpX = doc.x;
		doc
			.fillColor(fontColors.secondary)
			.fontSize(fontSizes.text)
			.text(`${job.company}`, {
				underline: true,
				link: job.website,
				columns: 1,
				width: 325,
			});
		doc
			.fillColor(fontColors.secondary)
			.fontSize(fontSizes.small)
			.text(job.position, 300, titleY + 3, {
				columns: 1,
				width: 250,
				align: "right",
			});

		doc.x = tmpX;

		doc
			.fillColor(fontColors.primary)
			.fontSize(fontSizes.small)
			.text(job.summary, { columns: 1, width: 325, align: "left" });
		doc
			.fillColor(fontColors.secondary)
			.fontSize(fontSizes.small)
			.text(`(${job.startDate} - ${job.endDate || "now"})`, {
				columns: 1,
				width: 300,
				align: "right",
			});
		doc.y += 5;
	}

	doc.y += 10;
	doc
		.fontSize(fontSizes.subtitle)
		.fillColor(fontColors.primary)
		.text("Projects");

	for (const project of data.resume.projects) {
		doc
			.fillColor(fontColors.secondary)
			.fontSize(fontSizes.text)
			.text(project.name, {
				columns: 1,
				width: 325,
				align: "left",
				underline: true,
				link: project.url,
			});

		doc
			.fillColor(fontColors.primary)
			.fontSize(fontSizes.small)
			.text(project.description, { columns: 1, width: 325, align: "left" });
		doc.y += 5;
	}

	doc.y += 10;
	doc
		.fontSize(fontSizes.subtitle)
		.fillColor(fontColors.primary)
		.text("Publications");

	for (const publication of data.resume.publications) {
		doc
			.fillColor(fontColors.secondary)
			.fontSize(fontSizes.text)
			.text(publication.name, {
				columns: 1,
				width: 325,
				align: "left",
				underline: true,
				link: publication.url,
			});

		doc
			.fillColor(fontColors.primary)
			.fontSize(fontSizes.small)
			.text(publication.summary, { columns: 1, width: 325, align: "left" });
		doc.y += 5;
	}
};

export async function buildPdf(data) {
	const doc = new PDFDocument({
		info: {
			Author: `${data.resume.basics.name} <${data.resume.basics.email}>`,
			Title: `${data.resume.basics.name}'s CV`,
			Subject: "Resume",
			Keywords: "resume, cv, portfolio, personal",
			ModDate: data.resume.meta.lastModified,
		},
		size: "A4",
		layout: "portrait",
		margin: 20,
		pdfVersion: "1.7",
	});
	doc.pipe(
		createWriteStream(getPathFromRoot(`${OUTPUT_VERSION}/resume.pdf`), {
			encoding: "utf-8",
		}),
	);

	doc
		.font(getPathFromRoot("./src/pdf/jetbrains-mono.ttf"))
		.fillColor(fontColors.primary);

	// title
	doc.fontSize(fontSizes.title).text(data.resume.basics.name);
	doc
		.fontSize(fontSizes.subtitle)
		.fillColor(fontColors.secondary)
		.text(data.resume.basics.label);

	// left column
	const contentYStart = createLeftColumn(doc, data);

	// right column
	createRightColumn(doc, data, contentYStart);

	// version
	const versionY =
		doc.page.height - doc.page.margins.top - doc.page.margins.bottom;
	const pageWidth =
		doc.page.width - doc.page.margins.left - doc.page.margins.right;
	doc
		.fontSize(fontSizes.small)
		.fillColor(fontColors.secondary)
		.text(
			`last updated: ${data.resume.meta.lastModified} - ${data.resume.meta.version}`,
			doc.page.margins.left,
			versionY,
			{ columns: 1, width: pageWidth, align: "right" },
		);

	doc.end();
}
