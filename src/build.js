import { mkdir, rm } from "node:fs/promises";

import { buildHtml } from "./html/html.js";
import { buildJson } from "./json/json.js";
import { buildPdf } from "./pdf/pdf.js";
import { OUTPUT, OUTPUT_VERSION, getPathFromRoot } from "./utils.js";

async function main() {
	const type = process.argv[2];

	console.info("🏗️ Building CV\n");

	const outPath = getPathFromRoot(OUTPUT);
	const outVersionPath = getPathFromRoot(OUTPUT_VERSION);
	await rm(outPath, { recursive: true, force: true });
	await mkdir(outVersionPath, { recursive: true });

	const jsonResume = await buildJson();
	switch (type) {
		case "json":
			// builds every time
			break;
		case "html":
			await buildHtml(jsonResume);
			break;
		case "pdf":
			await buildPdf(jsonResume);
			break;
		case undefined:
			await buildHtml(jsonResume);
			await buildPdf(jsonResume);
			break;
		default:
			throw new Error(
				`💥 Unknown operation type: ${type} (supported: json, html, pdf or none for all)`,
			);
	}
	console.info("✅ CV built in");
}

main().catch((err) => {
	console.error(err);
});
