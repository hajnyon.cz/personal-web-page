import { cp, mkdir, readFile, writeFile } from "node:fs/promises";
import { minify } from "html-minifier";
import { transform } from "lightningcss";
import mustache from "mustache";
import { OUTPUT, OUTPUT_WELL_KNOWN, getPathFromRoot } from "../utils.js";

const TEMPLATE = "./src/html/index.mustache";
const OUT = "./public/index.html";

export async function buildHtml(data) {
	// generate html
	const template = await readFile(getPathFromRoot(TEMPLATE), "utf8");
	const htmlOutput = mustache.render(template, data);

	// minify html
	const minifiedHtmlOutput = await minify(htmlOutput, {
		collapseWhitespace: true,
		removeComments: true,
	});

	// write the output
	const outPath = getPathFromRoot(OUT);
	await writeFile(outPath, minifiedHtmlOutput);
	await cp(getPathFromRoot("./src/html/static"), getPathFromRoot(OUTPUT), {
		recursive: true,
	});

	// minify css
	const cssFiles = [
		"./public/assets/css/reset.css",
		"./public/assets/css/monospace.css",
	];
	for (const cssFile of cssFiles) {
		const cssFilePath = getPathFromRoot(cssFile);
		const { code, warnings } = transform({
			filename: cssFile.split("/").pop(),
			code: await readFile(cssFilePath),
			minify: true,
		});
		if (warnings.length > 0) {
			console.warn(warnings);
		}
		await writeFile(cssFilePath, code);
	}

	// cp well-known files
	const wellKnownPath = getPathFromRoot(OUTPUT_WELL_KNOWN);
	await mkdir(wellKnownPath, { recursive: true });
	await cp(getPathFromRoot("./.well-known"), wellKnownPath, {
		recursive: true,
	});
}
